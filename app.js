var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var bodyParser = require('body-parser');
var path = require('path');
var exphbs = require('express3-handlebars');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

app.use(express.static(path.join(__dirname, 'public/images')));
app.use(express.static(path.join(__dirname, 'public/images/carousel')));
app.use(express.static(path.join(__dirname, 'public/js')));
app.use(express.static(path.join(__dirname, 'public/modules')));
app.use(express.static(path.join(__dirname, 'public/modules/viewModels')));
app.use(express.static(path.join(__dirname, 'public/css')));
app.use(express.static(path.join(__dirname, 'public/fonts')));
app.use(express.static(path.join(__dirname, 'views/templates')));

app.get('/', function(req, res){	
  res.render('home');
});

app.get('/genres', function(req, res){
  res.render('genres');  
});

app.get('/editions', function(req, res){
  res.render('editions');  
});

// Users
var users = require('./modules/users/index');
io.on('connection', function (socket) {  
  socket.on('loginUser', function (data) {     
    users.loginUser(data).then(function(result){
      // sending to sender-client only
      socket.emit('loginResponse', result);
    });    
  });

});

server.listen(8000, function(){
  console.log('Escuchando en el puerto 8000');
});