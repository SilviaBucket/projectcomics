var Storage = {
	// Retrieve item
	getSessionStorage: function(key){
		return sessionStorage.getItem(key);
	},
	// Save a value
	setSessionStorage: function(key, value){
		sessionStorage.setItem(key, value);
	},
	// Remove the key
	deleteSessionStorage: function(key){
		sessionStorage.removeItem(key);
	}
}