
var Autenthication = {
	loginSuccess: function(username){
        SessionStorage.setSessionUser(username);
        // Display a success toast, with a title
        toastr.options.positionClass = 'toast-top-center';      
        toastr.success('Login success', 'Login', { closeButton: true });
    },
    loginFail: function(){        
        // Display an error toast, with a title
        toastr.options.positionClass = 'toast-top-center';
        toastr.error('User or password incorrect', 'Login', { closeButton: true })
    }        
}