
// Namespace for the library
var UserViewModel = {};
 
// Library definition
UserViewModel = (function () {
    // Private variables / properties
    var vmUser;

    function viewModel(){
    	var self = this;
		self.userName = ko.observable("storres");
		self.userPassword = ko.observable("storres1234");	
		self.userLogged = ko.observable(false);				
		self.login = function() {
			var userData = {
				username: self.userName(),
				password: self.userPassword()
			};
			Socket.sendLoginMessage(userData);        
		};
		self.logout = function() {
			SessionStorage.deleteSessionUser("username");
			self.userLogged(false);
		};	
	};

    // Private methods
    function init() {
    	vmUser = new viewModel();
    	vmUser.userLogged(SessionStorage.existsSessionUser());        
        ko.applyBindings(vmUser, $("#headerView")[0]);
		ko.applyBindings(vmUser, $("#templateLogin")[0]);
    }

    function setUserLogged(value){
    	vmUser.userLogged(value);
    }
 
  // Public API
  return {
    init: init,
    setUserLogged: setUserLogged
  }   
});