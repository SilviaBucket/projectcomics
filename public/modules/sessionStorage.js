var SessionStorage = {
	// Users
	getSessionUser: function(){
		return Storage.getSessionStorage("username");
	},
    setSessionUser: function (username) {        
        Storage.setSessionStorage("username", username);        
    },
    deleteSessionUser: function(){
    	Storage.deleteSessionStorage("username");
    },
    existsSessionUser: function(){
    	var userSession = this.getSessionUser("username");
    	return userSession != "" && userSession != null && userSession != undefined;
    }
}