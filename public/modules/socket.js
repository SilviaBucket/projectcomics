
// Namespace for the library
var Socket = {};
 
// Library definition
Socket = (function () {
    // Private variables / properties
    var socket = io.connect('http://localhost:8000');

    // Private methods
    function init() {
        socket.on('loginResponse', function (data) {
            data.status ? Autenthication.loginSuccess(data.username) : Autenthication.loginFail();
            Comic.modelUser.setUserLogged(data.status);            
        });
    }

    function sendLoginMessage(dataLogin) {
        socket.emit('loginUser', dataLogin);
    }
 
  // Public API
  return {
    init: init,
    sendLoginMessage: sendLoginMessage
  }
}());

Socket.init();