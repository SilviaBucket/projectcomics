// mongod --dbpath D:\practicasfullstack\projectcomics\test\data\

// Requiero el driver
var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://localhost:27017/test';
var pBase = MongoClient.connect(url); // promise

pBase.then(function (base) {	
	// base ya es la conexion
    var users = base.collection('users');        
    users.drop().then(function(result){}).catch(populateDB(base));
}).catch(console.log("Database connection fail"));

exports.loginUser = function(data){	
	return pBase.then(function (base) {    	
		var users = base.collection('users');		
		var pLogin = users.find({ username: data.username, password: data.password }).toArray();		
		return pLogin.then(function(result) {			
			return {
				status: result.length > 0,
				username: data.username
			};
	    });
    });	
}

function populateDB(base) {
    var usersInsert = [
        {"name": "Silvia", "lastName": "Torres", "username": "storres", "password": "storres1234", "cellPhone": "617-000-0005", "email": "storres@gmail.com", "city": "Boston, MA"},
        {"name": "Ray", "lastName": "Moore", "username": "ray", "password": "rmoore1234", "cellPhone": "617-000-0005", "email": "storres@gmail.com", "city": "Boston, MA"}
    ];    
    var users = base.collection('users');

    var pInsert = users.insert(usersInsert);
    pInsert.then(function() {});

    /*var pFindAll = users.find({}).toArray();
	pFindAll.then(function(result) {
	    console.log('Los usuarios son: ', result);	    
	});*/
};