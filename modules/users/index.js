var dal = require('./dal');

function loginUser(data){	
	return dal.loginUser(data).then(function(result){		
		return result;      
    });    
}

module.exports = {
	loginUser: loginUser
};